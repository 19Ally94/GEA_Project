﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SoundManager : Singleton<SoundManager> {

    public SoundDataBase soundDataBase;
    public AudioSource audioSourceEnvironment;

    private Scene OldScene;
    private Scene actualScene;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        ChangeEnvironmentSounds();
    }

    private void Update()
    {
        actualScene = SceneManager.GetActiveScene();

        if (actualScene != OldScene)
        {
            OldScene = actualScene;
            ChangeEnvironmentSounds();
        }
    }

    private void ChangeEnvironmentSounds()
    {
        Scene scene = SceneManager.GetActiveScene();

        if (scene.buildIndex == 0)
        {
            audioSourceEnvironment.clip = soundDataBase.clipMenù;
            AudioSource Environment = Instantiate(audioSourceEnvironment, transform.position, Quaternion.identity);
            Environment.Play();
            Environment.volume = 0.19f;
        }
        else if (scene.buildIndex == 1)
        {
            audioSourceEnvironment.clip = soundDataBase.clipArena;
            AudioSource Environment = Instantiate(audioSourceEnvironment, transform.position, Quaternion.identity);
            Environment.Play();
            Environment.volume = 0.19f;
        }
    }

    
}
