﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

    [SerializeField]
    private float fillAmmountBlue;

    [SerializeField]
    private Image contentBlue;

    [SerializeField]
    private float fillAmmountRed;

    [SerializeField]
    private Image contentRed;

    public Image p1Life;
    public Image p2Life;
    public Button menuButton;
    public Button revengeButton;

    public UIData uIData;


    private void Start()
    {

        //menuButton.onClick.AddListener(MenuGame);
        //revengeButton.onClick.AddListener(StartGame);
        PlayerController.CounterIncresed += Map;
        PlayerController.Damage += VisiveDecresedLife;
    }

    private void OnDestroy()
    {
        PlayerController.CounterIncresed -= Map;
        PlayerController.Damage -= VisiveDecresedLife;
    }

    private void Update()
    {
        HandleBar();
    }

    public void StartGame()
    {
        SceneManager.LoadScene("ForestArena");
    }

    public void MenuGame()
    {
        SceneManager.LoadScene(0);
    }

    public void QuitGame()
    {
        Application.Quit();
        Debug.Log("QUIT!");
    }
    
    private void HandleBar()
    {
        contentBlue.fillAmount = fillAmmountBlue;
        contentRed.fillAmount = fillAmmountRed;
    }

    public void Map(int ID,float value, float MaxValue)
    {
        if (ID == 1)
        {
            fillAmmountBlue = value / MaxValue;
        }
        else if (ID == 2)
        {
            fillAmmountRed = value / MaxValue;
        }
    }

    public void VisiveDecresedLife(int ID, float hP)
    {
        if (ID == 1)
        {
            if (hP == 0)
            {
                p1Life.GetComponent<Image>().sprite = uIData.Bluehp0;
            }
            else if (hP == 1)
            {
                p1Life.GetComponent<Image>().sprite = uIData.Bluehp1;
            }
            else if (hP == 2)
            {
                p1Life.GetComponent<Image>().sprite = uIData.Bluehp2;
            }
            else if (hP == 3)
            {
                p1Life.GetComponent<Image>().sprite = uIData.Bluehp3;
            }
            else if (hP == 4)
            {
                p1Life.GetComponent<Image>().sprite = uIData.Bluehp4;
            }
            else if (hP == 5)
            {
                p1Life.GetComponent<Image>().sprite = uIData.Bluehp5;
            }
        }
        else if (ID == 2)
        {
            if (hP == 0)
            {
                p2Life.GetComponent<Image>().sprite = uIData.Redhp0;
            }
            else if (hP == 1)
            {
                p2Life.GetComponent<Image>().sprite = uIData.Redhp1;
            }
            else if (hP == 2)
            {
                p2Life.GetComponent<Image>().sprite = uIData.Redhp2;
            }
            else if (hP == 3)
            {
                p2Life.GetComponent<Image>().sprite = uIData.Redhp3;
            }
            else if (hP == 4)
            {
                p2Life.GetComponent<Image>().sprite = uIData.Redhp4;
            }
            else if (hP == 5)
            {
                p2Life.GetComponent<Image>().sprite = uIData.Redhp5;
            }
        }
    }
}
