﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UIInGame : MonoBehaviour {


    public Image winImage;
    public UIData uIData;
    public GameObject InputManager;

    public Image CounterBarRed;
    public Image CounterBarBlue;
    public Image CounterBarDetails;
    public Image BlueHP;
    public Image RedHP;
    public Image CounterBarBorides;

    public GameObject buttonMenu;
    public GameObject buttonRevenge;
    public GameObject ParticleBlueWin;
    public GameObject ParticleRedWin;

    public float timerForWin = 10;
    public bool playerWin = false;

    private void Start()
    {
        PlayerController.Win += ChangeWinImage;
    }

    private void OnDestroy()
    {
        PlayerController.Win -= ChangeWinImage;
    }

    private void Update()
    {
        if (playerWin)
        {
            timerForWin -= Time.deltaTime;
        }
    }

    void ChangeWinImage(int ID)
    {
        if (ID == 1)
        {
            playerWin = true;

            InputManager.SetActive(false);

            if (timerForWin <= 0)
            {
                ParticleBlueWin.SetActive(true);
                winImage.sprite = uIData.WinBlue;
                winImage.enabled = true;
                buttonMenu.SetActive(true);
                buttonRevenge.SetActive(true);
                CounterBarRed.enabled = false;
                CounterBarBlue.enabled = false;
                CounterBarDetails.enabled = false;
                BlueHP.enabled = false;
                RedHP.enabled = false;
                CounterBarBorides.enabled = false;
            }
        }
        else
        {
            playerWin = true;

            InputManager.SetActive(false);

            if (timerForWin <= 0)
            {
                ParticleRedWin.SetActive(true);
                buttonMenu.SetActive(true);
                buttonRevenge.SetActive(true);
                winImage.sprite = uIData.WinRed;
                winImage.enabled = true;
                CounterBarRed.enabled = false;
                CounterBarBlue.enabled = false;
                CounterBarDetails.enabled = false;
                BlueHP.enabled = false;
                RedHP.enabled = false;
                CounterBarBorides.enabled = false;  
            }
        }
    }
}
