﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "SoundDataBase")]
public class SoundDataBase : ScriptableObject {

    public AudioClip clipArena;
    public AudioClip clipMenù;
    public AudioClip clipGameOver;
    public AudioClip clipShot;
}
