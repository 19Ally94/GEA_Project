﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SinGrenade : MonoBehaviour {

    [SerializeField]
    float MovementSpeed = 1.0f;
    [SerializeField]
    float SinWeight = 5.0f;
    [SerializeField]
    float SinSpeed = 1.0f;

    

    Vector3 startPoint;

    float timer = 0;

    private void Start()
    {
        
    }

    private void Update()
    {
        if (timer * SinSpeed <= Mathf.PI * 2)
        {            
            transform.position += transform.right * MovementSpeed * Time.deltaTime;
            transform.position += transform.up * Mathf.Sin(timer * SinSpeed) * Time.deltaTime * SinSpeed;
            timer += Time.deltaTime;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
}
