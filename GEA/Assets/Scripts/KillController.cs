﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class KillController : MonoBehaviour {

    public static event Action<int> AddKillPoint;

    private void Start()
    {
        PlayerController.PlayerKill += KillerIdentifier;
    }

    private void OnDestroy()
    {
        PlayerController.PlayerKill -= KillerIdentifier;
    }

    void KillerIdentifier(int ID)
    {
        if (ID == 1)
        {
            AddKillPoint(2);
        }
        else
        {
            AddKillPoint(1);
        }
    }
}
