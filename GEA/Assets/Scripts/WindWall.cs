﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindWall : Grenade {


    grenadeStates actualState;
    Vector3 actualPosition;
    public LayerMask wall;
    [SerializeField] float elevation = 3.0f;
    [SerializeField] float lenght = 1.0f;
    [SerializeField] float ShootTime = 1.0f;
    float timer;

    private void Start()
    {
        
    }

    private void Update()
    {
        
    }

    public void WindWallFIre()
    {
        switch (actualState)
        {
            case grenadeStates.Movement:
                {
                    break;
                }
            case grenadeStates.Explosion:
                {
                    break;
                }
            case grenadeStates.Effect:
                {
                    break;
                }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Ground"))
        {
            actualPosition = transform.position;
            actualState = grenadeStates.Explosion;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, explosionRadious);
    }



}
