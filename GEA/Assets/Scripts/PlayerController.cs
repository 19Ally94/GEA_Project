﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerController : MonoBehaviour {

    public static event Action<int> PlayerKill;
    public static event Action<int> Win;
    public static event Action BulletSpawn;
    public static event Action<int,float,float> CounterIncresed;
    public static event Action<int, float> Damage;

    public float speed;
    public float KillPoint;
    public float Maxcounter;
    public float rotationSpeed;
    public float counter = 0;
    public float actualCounter = 0;
    public GameObject weapon;
    public GameObject bullet;
    public Transform bulletSpawnPosition;
    public float delay;
    public float grenadeShotDelay;
    public float hP;
    public float damage;
    public float explosionDamage;
    public GameObject WinlaserBlue;
    public GameObject WinlaserRed;


    public float timeOfGranadeAnimation;
    private Vector3 spawn;
    private float respawnTime = 2;
    private GameManager gM;
    private Rigidbody rb;
    private float timerBullet = 0;
    private float timerGrenade = 0;
    public int playerID;
    public int enemyPlayerID;
    public GameObject grenadePrefab;
    public Transform grenadeMuzzle;
    RewiredUtility rwUtility;
    int motorIndex = 0; // the first motor
    int motorIndex2 = 1;// the second motor
    float motorLevel = 0.5f; // full motor speed
    float duration = 0.3f; // 2 seconds


    private void Awake()
    {
        timerBullet = .5f;
        timerGrenade = 15f;

        rb = GetComponent<Rigidbody>();
        gM = FindObjectOfType<GameManager>();
    }

    private void Start()
    {
        rwUtility = FindObjectOfType<RewiredUtility>();

        if (Damage != null)
        {
            Damage(playerID, hP);
        }

        spawn = gameObject.transform.position;

        KillController.AddKillPoint += TakeKillPoint;
        CapturePoint.IncremeteBlueCounter += BlueCounterIncresed;
        CapturePoint.IncremeteRedCounter += RedCounterIncresed;
    }

    private void OnDestroy()
    {
        KillController.AddKillPoint -= TakeKillPoint;
        CapturePoint.IncremeteBlueCounter -= BlueCounterIncresed;
        CapturePoint.IncremeteRedCounter -= RedCounterIncresed;
    }

    private void Update()
    {

        if (counter != actualCounter)
        {
            if (CounterIncresed != null)
            {
                CounterIncresed(playerID, counter, Maxcounter);
            }
            actualCounter = counter;
        }

        timerBullet += Time.deltaTime;
        timerGrenade += Time.deltaTime;

        if (counter >= Maxcounter)
        {
            counter = Maxcounter;

            if (playerID == 1)
            {
                WinlaserBlue.gameObject.SetActive(true);

                if (Win != null)
                {
                    Win(playerID);
                }
            }
            else if (playerID == 2)
            {
                WinlaserRed.gameObject.SetActive(true);

                if (Win != null)
                {
                    Win(playerID);
                }
            }
        }
    }

    public void Move(Vector3 directions)
    {
        rb.velocity = speed * directions;  
    }

    public void Rotate(Vector3 directionsR)
    {
        Quaternion rotation = Quaternion.LookRotation(directionsR);

        weapon.transform.rotation = Quaternion.Slerp(weapon.transform.rotation, rotation, Time.deltaTime * rotationSpeed);
    }
    
    //Fuoco primario
    public void Fire()
    {
        if (timerBullet >= delay)
        {
            timerBullet = 0;
            GameObject bulletGO = Instantiate(bullet, bulletSpawnPosition.position, bulletSpawnPosition.rotation) as GameObject;
            //bulletGO.GetComponent<Bullet>().damage = damage;
        }
    }
    
    //Fuoco secondario
    public void Fire2()
    {
        if (timerGrenade >= grenadeShotDelay)
        {
            timerGrenade = 0;
            timeOfGranadeAnimation = 8;

            GameObject.Instantiate(grenadePrefab, grenadeMuzzle.position, grenadeMuzzle.rotation);
           // grenade.SetActive(true);
           // _grenade.FireGRenade();
            #region
            /*GameObject granadeGO = Instantiate(grenade, bulletSpawnPosition.position, bulletSpawnPosition.rotation + ) as GameObject;
            // _explosion.damage = explosionDamage;
            if (BulletSpawn != null)
            {
                BulletSpawn();
            }*/
            #endregion
        }
    }

    private void DeactiveGrenade()
    {

        timeOfGranadeAnimation -= Time.deltaTime;

        if (timeOfGranadeAnimation <= 0)
        {
            //grenade.SetActive(false);
        }

    }

    public void VelocityOff()
    {
        rb.velocity = Vector3.zero;
    }

    public void TakeDamage(float dmg)
    {
        hP -= dmg;

        if (Damage != null)
        {
            Damage(playerID, hP);
        }
        if (playerID==1)
        {
            rwUtility.GetComponent<RewiredUtility>().Rw_Player.SetVibration(motorIndex, motorLevel, duration);
        }
        else if (playerID==2)
        {
            rwUtility.GetComponent<RewiredUtility>().Rw2_Player.SetVibration(motorIndex2, motorLevel, duration);
        }

        if (hP <= 0)
        {
            if (PlayerKill != null)
            {
                PlayerKill(playerID);
            }

            gameObject.SetActive(false);
            
            Invoke("Respawn", respawnTime);
        }
    }

    void TakeKillPoint(int ID)
    {
        if (ID == playerID)
        {
            counter += KillPoint;
        }
    }

    public void TakeHp(float healt)
    {
        if (hP < 5)
        {
            hP += healt;
        }
        if (hP > 5)
        {
            hP = 5;
        }
    }

    void Respawn()
    {
        if (gameObject.activeSelf == false)
        {
            transform.position = spawn;
            hP = 5f;
            gameObject.SetActive(true);

            if (Damage != null)
            {
                Damage(playerID, hP);
            }
        }
    }
    
    void BlueCounterIncresed()
    {
        if (gameObject.tag=="Player1")
        {
            counter += 1;

            if (CounterIncresed != null)
            {
                CounterIncresed(playerID, counter, Maxcounter);
            }
        }
    }

    void RedCounterIncresed()
    {
        if (gameObject.tag == "Player2")
        {
            counter += 1;

            if (CounterIncresed != null)
            {
                CounterIncresed(playerID, counter, Maxcounter);
            }
        }
    }
}
