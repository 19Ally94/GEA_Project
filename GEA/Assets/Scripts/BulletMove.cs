﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BulletMove : MonoBehaviour {

    public static event Action<int, Vector3> BulletHit;
    public static event Action<int, Vector3> BulletHitPlayerBlue;
    public static event Action<int, Vector3> BulletHitPlayerRed;

    public float bulletSpeed;
    public Transform bulletSpawner;
    public float delay;
    public float damage;
    public int bulletID;
    
    

    private void Start()
    {
     
    }

    private void Update()
    {
        transform.Translate(bulletSpawner.right * bulletSpeed * Time.deltaTime);
        if (delay > 0)
        {
            delay -= Time.deltaTime;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Wall"))
        {
            if (BulletHit != null)
            {
                BulletHit(bulletID, transform.position);
            }
        }

        if (other.gameObject.CompareTag("Player1"))
        {
            other.gameObject.GetComponent<PlayerController>().TakeDamage(damage);
    


            if (BulletHitPlayerBlue != null)
            {
                BulletHitPlayerBlue(bulletID, transform.position);
            }
        }

        else if (other.gameObject.CompareTag("Player2"))
        {
            other.gameObject.GetComponent<PlayerController>().TakeDamage(damage);
            

            if (BulletHitPlayerRed != null)
            {
                BulletHitPlayerRed(bulletID, transform.position);
            }
        }

        Destroy(gameObject);
    }
}
