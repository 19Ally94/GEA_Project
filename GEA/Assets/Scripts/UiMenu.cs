﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class UiMenu : MonoBehaviour {

    public void StartNewGame()
    {
        SceneManager.LoadScene(1);
    }

    public void ExitToGame()
    {

    }

    public void OptionsOfGame()
    {

    }

    public void Credits()
    {

    }

}

