﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "EnvMaterialData")]
public class EnvMaterialData : ScriptableObject
{
    public Material enabledEmissiveMaterialColumBlue;
    public Material enabledEmissiveMaterialColumRed;
    public Material disableEmissiveMaterialColum;


    public Material enableEmissiveMaterialStoneBlue;
    public Material enableEmissiveMaterialStoneRed;
    public Material disableEmissiveMaterialStone;
}
