﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CapturePointTrigger : MonoBehaviour {

    //manages the contact between rune and playes

    public static event Action<int> PlayerBlueTouchedCollider;
    public static event Action<int> PlayerRedTouchedCollider;
    public static event Action PlayerNotTouchedCollider;
    

    //it serves to recognized whitch rune it was triggered
    public int RuneID;

    private void Start()
    {
        CapturePoint.DisableThisRune += ColiderOff;
        CapturePoint.EnableThisRune += ColiderOn;
    }

    private void OnDestroy()
    {
        CapturePoint.DisableThisRune += ColiderOff;
        CapturePoint.EnableThisRune += ColiderOn;
    }

    //send and input for start metod in CapturePointManager
    private void OnTriggerStay(Collider other)
    {
        //blue player triggered
        if (other.gameObject.CompareTag("Player1"))
        {
            if (PlayerBlueTouchedCollider != null)
            {
                PlayerBlueTouchedCollider(RuneID);
            }
        }
        //red player triggered
        else if (other.gameObject.CompareTag("Player2"))
        {
            if (PlayerRedTouchedCollider != null)
            {
                PlayerRedTouchedCollider(RuneID);
            }
        }
        //nobody touches the rune
    }

    private void OnTriggerExit(Collider other)
    {
        if (PlayerNotTouchedCollider != null)
        {
            PlayerNotTouchedCollider();
        }
    }

    private void ColiderOff(int ID)
    {
        if (RuneID == ID)
        {
            gameObject.GetComponent<Collider>().enabled = false;
        }
    }

    private void ColiderOn(int ID)
    { 
        if (RuneID == ID)
        {
            gameObject.GetComponent<Collider>().enabled = true;
        }
    }
}
