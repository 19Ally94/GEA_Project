﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class InputManager : MonoBehaviour {
    
    public PlayerController playerMove;
    public PlayerController player2Move;

    private RewiredUtility rw_Utilities;
    

    void Awake()
    {
        rw_Utilities = FindObjectOfType<RewiredUtility>();
    }

    void Update()
    {
        InputMove();
        InputRotation();
        InputFire();
        InputFire2();
    }
    void InputMove()
    {


        // PLAYER 1 MOVE
        if (Mathf.Abs(rw_Utilities.Rw_Player.GetAxis("MoveHorizontal")) >= 0.2f || Mathf.Abs(rw_Utilities.Rw_Player.GetAxis("MoveVertical")) >= 0.2f)
        {
            float moveHorizontal = rw_Utilities.Rw_Player.GetAxis("MoveHorizontal");
            float moveVertical = rw_Utilities.Rw_Player.GetAxis("MoveVertical");
            Vector3 directions = new Vector3(moveHorizontal, 0, moveVertical);
            if (playerMove != null)
            {
                playerMove.Move(directions);
            }
        }
        else
        {
            if (playerMove != null)
                playerMove.VelocityOff();
        }
        // PLAYER 2 MOVE
        if (Mathf.Abs(rw_Utilities.Rw2_Player.GetAxis("MoveHorizontal")) >= 0.2f || Mathf.Abs(rw_Utilities.Rw2_Player.GetAxis("MoveVertical")) >= 0.2f)
        {
            float moveHorizontal = rw_Utilities.Rw2_Player.GetAxis("MoveHorizontal");
            float moveVertical = rw_Utilities.Rw2_Player.GetAxis("MoveVertical");
            Vector3 directions = new Vector3(moveHorizontal, 0, moveVertical);
            if (player2Move != null)
                player2Move.Move(directions);
        }
        else
        {
            if (player2Move != null)
                player2Move.VelocityOff();
        }
    }
    void InputRotation()
    {
        // PLAYER 1 ROTATE
        if (Mathf.Abs(rw_Utilities.Rw_Player.GetAxis("RotationHorizontal")) >= 0.2f || Mathf.Abs(rw_Utilities.Rw_Player.GetAxis("RotationVertical")) >= 0.2f)
        {
            float moveHorizontal = -rw_Utilities.Rw_Player.GetAxis("RotationHorizontal");
            float moveVertical = rw_Utilities.Rw_Player.GetAxis("RotationVertical");
            Vector3 directions = new Vector3(moveHorizontal, 0, moveVertical);
            if (playerMove != null)
                playerMove.Rotate(directions);
        }
        // PLAYER 2 ROTATE
        if (Mathf.Abs(rw_Utilities.Rw2_Player.GetAxis("RotationHorizontal")) >= 0.2f || Mathf.Abs(rw_Utilities.Rw2_Player.GetAxis("RotationVertical")) >= 0.2f)
        {
            float moveHorizontal = rw_Utilities.Rw2_Player.GetAxis("RotationHorizontal");
            float moveVertical = -rw_Utilities.Rw2_Player.GetAxis("RotationVertical");
            Vector3 directions = new Vector3(moveHorizontal, 0, moveVertical);
            if (player2Move != null)
                player2Move.Rotate(directions);
        }
    }
    void InputFire()
    {
        // PLAYER 1 FIRE
        if (Mathf.Abs(rw_Utilities.Rw_Player.GetAxis("Fire")) >= 0.5f)
        {
            if (playerMove != null)
                playerMove.Fire();
        }
        // PLAYER 2 FIRE
        if (Mathf.Abs(rw_Utilities.Rw2_Player.GetAxis("Fire")) >= 0.5f)
        {
            if (player2Move != null)
                player2Move.Fire();
        }

    }
    void InputFire2()
    {
        // PLAYER 1 FIRE2
        if (Mathf.Abs(rw_Utilities.Rw_Player.GetAxis("Fire2")) >= 0.5f)
        {
            if (playerMove != null)
                playerMove.Fire2();

        }
        // PLAYER 2 FIRE2
        if (Mathf.Abs(rw_Utilities.Rw2_Player.GetAxis("Fire2")) >= 0.5f)
        {
            if (player2Move != null)
                player2Move.Fire2();
        }
    }


}
