﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(menuName = "UIData")]
public class UIData : ScriptableObject
{
    public Sprite Bluehp0;
    public Sprite Bluehp1;
    public Sprite Bluehp2;
    public Sprite Bluehp3;
    public Sprite Bluehp4;
    public Sprite Bluehp5;
    public Sprite Redhp0;
    public Sprite Redhp1;
    public Sprite Redhp2;
    public Sprite Redhp3;
    public Sprite Redhp4;
    public Sprite Redhp5;


    public Sprite WinBlue;
    public Sprite WinRed;

}

