﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class RewiredUtility : MonoBehaviour {
    private Rewired.Player rw_Player;
    public Rewired.Player Rw_Player { get { return rw_Player; } }
    private RewiredUtility rw_Utilities;
    private Rewired.Player rw2_Player;
    public Rewired.Player Rw2_Player { get { return rw2_Player; } }

    private void Awake()
    {
        rw_Utilities = transform.GetComponent<RewiredUtility>();
        rw_Player = ReInput.players.GetPlayer(0);
        rw2_Player = ReInput.players.GetPlayer(1);

    }
}
