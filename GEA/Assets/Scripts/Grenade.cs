﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Grenade : MonoBehaviour
{
     public enum grenadeStates { Movement, Explosion, Effect }

      grenadeStates actualState = grenadeStates.Movement;

    public LayerMask playerMask;
    public float explosionRadious;    
    float fireCarpetDuration;
    List<Collider> DamageList;

    private Vector3 actualPosition;

    [SerializeField] float elevation = 3.0f;
    [SerializeField] float lenght = 1.0f;
    [SerializeField] float ShootTime = 1.0f;

    Vector3 StartPosition;
    float timer;
    float otherTimer;

    private void Start()
    {
        StartPosition = transform.position;
        timer = 0.0f;
        actualState = grenadeStates.Movement;
    }

    private void Update()
    {
        
        timer += Time.deltaTime;
        switch (actualState)
        {
            case grenadeStates.Movement:
                {

                    float newYPos = (-(Mathf.Pow(timer, 2)) + (timer * ShootTime)) * elevation;

                    transform.position = StartPosition + (transform.TransformDirection(Vector3.up) * newYPos) + (transform.TransformDirection(Vector3.right) * timer * lenght);


                    break;

                }
            case grenadeStates.Explosion:
                {
                    transform.position = actualPosition;

                    Collider[] ColliderDamage = Physics.OverlapSphere(transform.position, explosionRadious, playerMask);

                    for (int i = 0; i < ColliderDamage.Length; i++)
                    {

                        //applico 5 danni
                        PlayerController ControllerToDamage = ColliderDamage[i].GetComponent<PlayerController>();
                        if (ControllerToDamage)
                        {
                            ControllerToDamage.TakeDamage(5f);
                        }
                        Debug.Log("PLAYER DAMAGED");

                    }
                    timer = 0;
                    otherTimer = 0;

                    actualState = grenadeStates.Effect;
                    break;
                }
            case grenadeStates.Effect:
                {

                    if (timer <= 2.0f)
                    {
                        if (otherTimer >= 1.0f)
                        {
                            otherTimer = 0.0f;
                            fireCarpetDuration += Time.deltaTime;
                            Collider[] IsPlayerInside = Physics.OverlapSphere(transform.position, explosionRadious, playerMask);
                            for (int i = 0; i < IsPlayerInside.Length; i++)
                            {
                                PlayerController ControllerToDamage = IsPlayerInside[i].GetComponent<PlayerController>();
                                if (ControllerToDamage)
                                {
                                    ControllerToDamage.TakeDamage(1f);
                                }
                            }
                        }
                        otherTimer += Time.deltaTime;
                    }
                    else
                    {
                        Destroy(this.gameObject);
                    }
                    break;
                }
        }

    }
    void FireGrenadeFire()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Ground"))
        {
            actualPosition = transform.position;
            actualState = grenadeStates.Explosion;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, explosionRadious);
    }

    float map(float s, float a1, float a2, float b1, float b2)
    {
        return b1 + (s - a1) * (b2 - b1) / (a2 - a1);
    }
    /*
public float firingAngle = 45.0f;
public float gravity = 9.8f;
public Transform grenade;

[SerializeField]
private GameObject bulletSpawn;

[SerializeField]
private GameObject target;

private Transform actualPositionTarget;
private Transform actualPositionPlayer;

private bool positionSaved = false;

private void Update()
{
    if (positionSaved==false)
    {
        SavePosition();
    }
    //Fire();
}
public void SavePosition()
{
    actualPositionTarget = target.transform;
    actualPositionPlayer = bulletSpawn.transform;
    positionSaved = true;
}
#region
// Use this for initialization

//public void Fire()
//{
//    StartCoroutine(GrenadeMovement());
//}
//IEnumerator GrenadeMovement()
//{


//    grenade.position = bulletSpawn.transform.position;
//    float targetDistance = Vector3.Distance(grenade.position, target.transform.position);
//    float grenadeVelocity = targetDistance / (Mathf.Sin(2 * firingAngle * Mathf.Deg2Rad)/ gravity);

//    // Extract the X  Y componenent of the velocity
//    float Vx = Mathf.Sqrt(grenadeVelocity) * Mathf.Cos(firingAngle * Mathf.Deg2Rad);
//    float Vy = Mathf.Sqrt(grenadeVelocity) * Mathf.Sin(firingAngle * Mathf.Deg2Rad);


//    // Calculate flight time.
//    float flightDuration = targetDistance / Vx;

//    // Rotate projectile to face the target.
//    grenade.rotation = Quaternion.LookRotation(target.transform.position - grenade.position);

//    float elapse_time = 0;

//    while (elapse_time < flightDuration)
//    {
//        grenade.Translate(0, (Vy - (gravity * elapse_time)) * Time.deltaTime, Vx * Time.deltaTime);

//        elapse_time += Time.deltaTime;

//        yield return null;
//    }


//}
#endregion
private void OnTriggerEnter(Collider other)
{
    if (other.gameObject.CompareTag("Wall"))
    {
        Destroy(this.gameObject);
    }
}
*/
}
