﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Environment : MonoBehaviour {

    public GameObject columnTopBlue;
    public GameObject columnBotBlue;
    public GameObject columnTopRed;
    public GameObject columnBotRed;

    public GameObject stoneTopBlue;
    public GameObject stoneBotBlue;
    public GameObject stoneTopRed;
    public GameObject stoneBotRed;

    public GameObject stoneBotBlueTop;
    public GameObject stoneMidBlueTop;
    public GameObject stoneTopBlueTop;
    public GameObject gemBlueTop;
    public GameObject lightBlueTop;
    public GameObject smokeBlueTop;

    public GameObject stoneBotBlueBot;
    public GameObject stoneMidBlueBot;
    public GameObject stoneTopBlueBot;
    public GameObject gemBlueBot;
    public GameObject lightBlueBot;
    public GameObject smokeBlueBot;

    public GameObject stoneBotRedTop;
    public GameObject stoneMidRedTop;
    public GameObject stoneTopRedTop;
    public GameObject gemRedTop;
    public GameObject lightRedTop;
    public GameObject smokeRedTop;

    public GameObject stoneBotRedBot;
    public GameObject stoneMidRedBot;
    public GameObject stoneTopRedBot;
    public GameObject gemRedBot;
    public GameObject lightRedBot;
    public GameObject smokeRedBot;

    public EnvMaterialData envMaterialData;

    private float timerEnableSonBlueTop = 5;
    private float timerEnableSonBlueBot = 5;
    private float timerEnableSonRedTop = 5;
    private float timerEnableSonRedBot = 5;

    private bool boolEnableSonBlueTop = false;
    private bool boolEnableSonBlueBot = false;
    private bool boolEnableSonRedTop = false;
    private bool boolEnableSonRedBot = false;

    private bool boolEnableSonLightBlueTop = false;
    private bool boolEnableSonLightBlueBot = false;
    private bool boolEnableSonLightRedTop = false;
    private bool boolEnableSonLightRedBot = false;


    private void Start()
    {
        stoneBotBlueTop = stoneTopBlue.gameObject.gameObject.transform.GetChild(0).gameObject;
        stoneMidBlueTop = stoneTopBlue.gameObject.gameObject.transform.GetChild(1).gameObject;
        stoneTopBlueTop = stoneTopBlue.gameObject.gameObject.transform.GetChild(2).gameObject;
        gemBlueTop = stoneTopBlue.gameObject.gameObject.transform.GetChild(3).gameObject;
        lightBlueTop = stoneTopBlue.gameObject.gameObject.transform.GetChild(4).gameObject;
        smokeBlueTop = stoneTopBlue.gameObject.gameObject.transform.GetChild(5).gameObject;

        stoneBotBlueBot = stoneBotBlue.gameObject.gameObject.transform.GetChild(0).gameObject;
        stoneMidBlueBot = stoneBotBlue.gameObject.gameObject.transform.GetChild(1).gameObject;
        stoneTopBlueBot = stoneBotBlue.gameObject.gameObject.transform.GetChild(2).gameObject;
        gemBlueBot = stoneBotBlue.gameObject.gameObject.transform.GetChild(3).gameObject;
        lightBlueBot = stoneBotBlue.gameObject.gameObject.transform.GetChild(4).gameObject;
        smokeBlueBot = stoneBotBlue.gameObject.gameObject.transform.GetChild(5).gameObject;

        stoneBotRedBot = stoneBotRed.gameObject.gameObject.transform.GetChild(0).gameObject;
        stoneMidRedBot = stoneBotRed.gameObject.gameObject.transform.GetChild(1).gameObject;
        stoneTopRedBot = stoneBotRed.gameObject.gameObject.transform.GetChild(2).gameObject;
        gemRedBot = stoneBotRed.gameObject.gameObject.transform.GetChild(3).gameObject;
        lightRedBot = stoneBotRed.gameObject.gameObject.transform.GetChild(4).gameObject;
        smokeRedBot = stoneBotRed.gameObject.gameObject.transform.GetChild(5).gameObject;

        stoneBotRedTop = stoneTopRed.gameObject.gameObject.transform.GetChild(0).gameObject;
        stoneMidRedTop = stoneTopRed.gameObject.gameObject.transform.GetChild(1).gameObject;
        stoneTopRedTop = stoneTopRed.gameObject.gameObject.transform.GetChild(2).gameObject;
        gemRedTop = stoneTopRed.gameObject.gameObject.transform.GetChild(3).gameObject;
        lightRedTop = stoneTopRed.gameObject.gameObject.transform.GetChild(4).gameObject;
        smokeRedTop = stoneTopRed.gameObject.gameObject.transform.GetChild(5).gameObject;

        PlayerController.CounterIncresed += ActiveEmissiveEnvironment;
    }

    private void Update()
    {
        SonEnabled();
        LightEnabled();
    }

    private void OnDestroy()
    {
        PlayerController.CounterIncresed += ActiveEmissiveEnvironment;
    }

    void ActiveEmissiveEnvironment(int iD, float counter, float maxCounter)
    {
        if (iD == 1)
        {
            if (counter >= 1000 && counter < 2000)
            {
                columnTopBlue.GetComponent<Renderer>().material = envMaterialData.enabledEmissiveMaterialColumBlue;
            }
            else if (counter >= 2000 && counter < 3000)
            {
                columnBotBlue.GetComponent<Renderer>().material = envMaterialData.enabledEmissiveMaterialColumBlue;
            }
            else if (counter >= 3000 && counter < 4000)
            {
                stoneBotBlueTop.GetComponent<Renderer>().material = envMaterialData.enableEmissiveMaterialStoneBlue;
                stoneMidBlueTop.GetComponent<Renderer>().material = envMaterialData.enableEmissiveMaterialStoneBlue;
                stoneTopBlueTop.GetComponent<Renderer>().material = envMaterialData.enableEmissiveMaterialStoneBlue;

                stoneMidBlueTop.GetComponent<Animator>().enabled = true;
                stoneTopBlueTop.GetComponent<Animator>().enabled = true;

                boolEnableSonBlueTop = true;

            }
            else if (counter >= 4000 && counter < 5000)
            {
                stoneBotBlueBot.GetComponent<Renderer>().material = envMaterialData.enableEmissiveMaterialStoneBlue;
                stoneMidBlueBot.GetComponent<Renderer>().material = envMaterialData.enableEmissiveMaterialStoneBlue;
                stoneTopBlueBot.GetComponent<Renderer>().material = envMaterialData.enableEmissiveMaterialStoneBlue;

                stoneMidBlueBot.GetComponent<Animator>().enabled = true;
                stoneTopBlueBot.GetComponent<Animator>().enabled = true;

                boolEnableSonBlueBot = true;

            }
        }
        else if (iD == 2)
        {
            if (counter >= 1000 && counter < 2000)
            {
                columnTopRed.GetComponent<Renderer>().material = envMaterialData.enabledEmissiveMaterialColumRed;
            }
            else if (counter >= 2000 && counter < 3000)
            {
                columnBotRed.GetComponent<Renderer>().material = envMaterialData.enabledEmissiveMaterialColumRed;
            }
            else if (counter >= 3000 && counter < 4000)
            {
                stoneBotRedTop.GetComponent<Renderer>().material = envMaterialData.enableEmissiveMaterialStoneRed;
                stoneMidRedTop.GetComponent<Renderer>().material = envMaterialData.enableEmissiveMaterialStoneRed;
                stoneTopRedTop.GetComponent<Renderer>().material = envMaterialData.enableEmissiveMaterialStoneRed;

                stoneMidRedTop.GetComponent<Animator>().enabled = true;
                stoneTopRedTop.GetComponent<Animator>().enabled = true;

                boolEnableSonRedTop = true;

            }
            else if (counter >= 4000 && counter < 5000)
            {
                stoneBotRedBot.GetComponent<Renderer>().material = envMaterialData.enableEmissiveMaterialStoneRed;
                stoneMidRedBot.GetComponent<Renderer>().material = envMaterialData.enableEmissiveMaterialStoneRed;
                stoneTopRedBot.GetComponent<Renderer>().material = envMaterialData.enableEmissiveMaterialStoneRed;

                stoneMidRedBot.GetComponent<Animator>().enabled = true;
                stoneTopRedBot.GetComponent<Animator>().enabled = true;

                boolEnableSonRedBot = true;

            }
        }
    }

    void SonEnabled()
    {
        if (boolEnableSonBlueTop == true)
        {
            timerEnableSonBlueTop -= Time.deltaTime;

            if (timerEnableSonBlueTop <= 3)
            {
                boolEnableSonLightBlueTop = true;
                smokeBlueTop.SetActive(true);
            }
            if (timerEnableSonBlueTop <= 0)
            {
                gemBlueTop.SetActive(true);
                boolEnableSonBlueTop = false;

            }
        }

        if (boolEnableSonRedTop == true)
        {
            timerEnableSonRedTop -= Time.deltaTime;

            if (timerEnableSonRedTop <= 3)
            {
                boolEnableSonLightRedTop = true;
                smokeRedTop.SetActive(true);
            }
            if (timerEnableSonRedTop <= 0)
            {
                gemRedTop.SetActive(true);
                boolEnableSonRedTop = false;

            }
        }

        if (boolEnableSonBlueBot == true)
        {
            timerEnableSonBlueBot -= Time.deltaTime;

            if (timerEnableSonBlueBot <= 3)
            {
                boolEnableSonLightBlueBot = true;
                smokeBlueBot.SetActive(true);
            }
            if (timerEnableSonBlueBot <= 0)
            {
                gemBlueBot.SetActive(true);
                boolEnableSonBlueBot = false;

            }
        }

        if (boolEnableSonRedBot == true)
        {
            timerEnableSonRedBot -= Time.deltaTime;

            if (timerEnableSonRedBot <= 3)
            {
                boolEnableSonLightRedBot = true;
                smokeRedBot.SetActive(true);
            }
            if (timerEnableSonRedBot <= 0)
            {
                gemRedBot.SetActive(true);
                boolEnableSonRedBot = false;

            }
        }
    }

    void LightEnabled()
    {
        if (boolEnableSonLightBlueTop)
        {
            lightBlueTop.SetActive(true);

            if (lightBlueTop.GetComponent<Light>().intensity < 2)
            {
                lightBlueTop.GetComponent<Light>().intensity += 1 * Time.deltaTime;
            }
            else
            {
                boolEnableSonLightBlueTop = false;
            }
        }

        if (boolEnableSonLightRedTop)
        {
            lightRedTop.SetActive(true);

            if (lightRedTop.GetComponent<Light>().intensity < 2)
            {
                lightRedTop.GetComponent<Light>().intensity += 1 * Time.deltaTime;
            }
            else
            {
                boolEnableSonLightRedTop = false;
            }
        }

        if (boolEnableSonLightBlueBot)
        {
            lightBlueBot.SetActive(true);

            if (lightBlueBot.GetComponent<Light>().intensity < 2)
            {
                lightBlueBot.GetComponent<Light>().intensity += 1 * Time.deltaTime;
            }
            else
            {
                boolEnableSonLightBlueBot = false;
            }

        }

        if (boolEnableSonLightRedBot)
        {
            lightRedBot.SetActive(true);

            if (lightRedBot.GetComponent<Light>().intensity < 2)
            {
                lightRedBot.GetComponent<Light>().intensity += 1 * Time.deltaTime;
            }
            else
            {
                boolEnableSonLightRedBot = false;
            }
        }
    }
}
