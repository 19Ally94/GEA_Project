﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamagePool : MonoBehaviour {

    public GameObject hitWallBlue;
    public GameObject hitWallRed;
    public GameObject hitPlayerBlue;
    public GameObject hitPlayerRed;

    public List<GameObject> particlesBlue = new List<GameObject>();
    public List<GameObject> particlesRed = new List<GameObject>();
    public List<GameObject> particlesBluePlayer = new List<GameObject>();
    public List<GameObject> particlesRedPlayer = new List<GameObject>();

    public int maxNumberOfObject;

    private void Awake()
    {
        for (int i = 0; i < maxNumberOfObject; i++)
        {
            GameObject damageBlueParticles = Instantiate(hitWallBlue, transform.position, Quaternion.identity);
            damageBlueParticles.SetActive(false);
            particlesBlue.Add(damageBlueParticles);
        }

        for (int i = 0; i < maxNumberOfObject; i++)
        {
            GameObject damageRedParticles = Instantiate(hitWallRed, transform.position, Quaternion.identity);
            damageRedParticles.SetActive(false);
            particlesRed.Add(damageRedParticles);
        }

        for (int i = 0; i < maxNumberOfObject; i++)
        {
            GameObject damageBlueParticles = Instantiate(hitPlayerBlue, transform.position, Quaternion.identity);
            damageBlueParticles.SetActive(false);
            particlesBluePlayer.Add(damageBlueParticles);
        }

        for (int i = 0; i < maxNumberOfObject; i++)
        {
            GameObject damageRedParticles = Instantiate(hitPlayerRed, transform.position, Quaternion.identity);
            damageRedParticles.SetActive(false);
            particlesRedPlayer.Add(damageRedParticles);
        }
    }

    private void Start()
    {
        BulletMove.BulletHit += ActiveAndPositionParticlesWall;
        BulletMove.BulletHitPlayerRed += ActiveAndPositionParticlesRed;
        BulletMove.BulletHitPlayerBlue += ActiveAndPositionParticlesBlue;
    }

    private void OnDestroy()
    {
        BulletMove.BulletHit -= ActiveAndPositionParticlesWall;
        BulletMove.BulletHitPlayerBlue -= ActiveAndPositionParticlesBlue;
        BulletMove.BulletHitPlayerRed -= ActiveAndPositionParticlesRed;
    }

    void ActiveAndPositionParticlesWall(int ID, Vector3 position)
    {
        if (ID == 1)
        {
            foreach (GameObject g in particlesBlue)
            {
                if (g.activeInHierarchy == false)
                {
                    g.transform.position = position;
                    g.SetActive(true);
                    break;
                }
            }
        }
        else
        {
            foreach (GameObject g in particlesRed)
            {
                if (g.activeInHierarchy == false)
                {
                    g.transform.position = position;
                    g.SetActive(true);
                    break;
                }
            }
        }
    }
    void ActiveAndPositionParticlesRed(int ID, Vector3 position)
    {
        foreach (GameObject g in particlesRedPlayer)
        {
            if (g.activeInHierarchy == false)
            {
                g.transform.position = position;
                g.SetActive(true);
                break;
            }
        }
    }
    void ActiveAndPositionParticlesBlue(int ID, Vector3 position)
    {

        foreach (GameObject g in particlesBluePlayer)
        {
            if (g.activeInHierarchy == false)
            {
                g.transform.position = position;
                g.SetActive(true);
                break;
            }
        }
    }
}
