﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HirParticlesController : MonoBehaviour {

    public float timerDisableObject;
    private Vector3 startedPosition;


    private void Start()
    {
        startedPosition = transform.position;
    }

    private void Update()
    {
        if (gameObject.activeInHierarchy == true)
        {
            timerDisableObject -= Time.deltaTime;

            if (timerDisableObject <= 0)
            {
                timerDisableObject = 2;
                transform.position = startedPosition;
                gameObject.SetActive(false);
            }
        }
    }

}
