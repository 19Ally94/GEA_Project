﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CapturePoint : MonoBehaviour
{

    public static event Action IncremeteBlueCounter;
    public static event Action IncremeteRedCounter;
    public static event Action<int> DisableThisRune;
    public static event Action<int> EnableThisRune;
    public static event Action BlueCapturRune;
    public static event Action RedCapturRune;

    
    public float ContestSpeed;
    public float timeToRestartRune;
    public float loseRoneSpeed;
    public float timeBeforeTheReconquest = 5f;

    float runeLife = 1000;
    float pointForCaptureRune = .5f;

    private bool InLerp = true;
    private bool lightEnableCapturd;
    private Light runeControlled;
    private GameObject runeCaptured;
    private GameObject runeDisputed;
    private GameObject runeDisputed2;
    private bool runeInContest;
    
    private int playerID = 0;
    private int actualPlayerID = 0;
    
    public int capturePointID;

    private void Start()
    {

        lightEnableCapturd = false;

        CapturePointTrigger.PlayerBlueTouchedCollider += BluePointContest;
        CapturePointTrigger.PlayerRedTouchedCollider += RedPointContest;
        CapturePointTrigger.PlayerBlueTouchedCollider += CallNameOfChilds;
        CapturePointTrigger.PlayerRedTouchedCollider += CallNameOfChilds;
        CapturePointTrigger.PlayerNotTouchedCollider += NobodyContestRune;
    }

    private void OnDestroy()
    {
        CapturePointTrigger.PlayerBlueTouchedCollider -= BluePointContest;
        CapturePointTrigger.PlayerRedTouchedCollider -= RedPointContest;
        CapturePointTrigger.PlayerBlueTouchedCollider -= CallNameOfChilds;
        CapturePointTrigger.PlayerRedTouchedCollider -= CallNameOfChilds;
        CapturePointTrigger.PlayerNotTouchedCollider -= NobodyContestRune;
    }

    private void Update()
    {

        if (lightEnableCapturd == true)
        {
            if (runeControlled.intensity < 30 && InLerp == true)
            {
                runeControlled.intensity += 1;
                runeControlled.range = 3;
            }
            else if(runeControlled.intensity >= 30)
            {
                InLerp = false;
            }

            if (runeControlled.intensity > 10 && InLerp == false)
            {
                
                runeControlled.intensity -= 1;
            }
            else if(runeControlled.intensity <= 10)
            {
                InLerp = true;
                runeControlled.range = 2;
                lightEnableCapturd = false;
            }
        }

        PointCaptured();
        ContestControl();

        if (timeToRestartRune <= 3 && timeToRestartRune >= 0.1)
        {
            gameObject.transform.GetChild(2).gameObject.SetActive(true);
        }
        else if(timeToRestartRune <= 0.1)
        {
            gameObject.transform.GetChild(2).gameObject.SetActive(false);
        }

        
        if (runeInContest == false)
        {
            RuneLoss();
        }
        else
        {
            timeBeforeTheReconquest = 10;
        }

        if (runeLife <= 0)
        {
            ResetRune();

            timeToRestartRune -= Time.deltaTime;

            if (timeToRestartRune <= 0)
            {
                runeLife = 1000;
                timeToRestartRune = 20;

                if (EnableThisRune != null)
                {
                    EnableThisRune(capturePointID);
                }
            }
        }
    }


    void PointCaptured()
    {
        if (pointForCaptureRune >= 1)
        {
            runeLife -= 1;

            if (IncremeteRedCounter != null)
            {
                IncremeteRedCounter();
            }

            if (RedCapturRune != null)
            {
                RedCapturRune();
            }
        }
        else if (pointForCaptureRune <= 0)
        {
            runeLife -= 1;

            if (IncremeteBlueCounter != null)
            {
                IncremeteBlueCounter();
            }

            if (BlueCapturRune != null)
            {
                BlueCapturRune();
            }
        }
    }

    void BluePointContest(int ID)
    {
        runeInContest = true;
        playerID = 1;
        if (capturePointID == ID)
        {
            pointForCaptureRune -= ContestSpeed;
        }
    }

    void RedPointContest(int ID)
    {
        runeInContest = true;
        playerID = 2;
        if (capturePointID == ID)
        {
            pointForCaptureRune += ContestSpeed;
        }
    }

    void NobodyContestRune()
    {
        runeInContest = false;
    }

    void RuneLoss()
    {
        if (pointForCaptureRune != .5f)
        {
            if (pointForCaptureRune >= 0.4866f || pointForCaptureRune <= 0.5159f)
            {
                if (pointForCaptureRune != 1f && pointForCaptureRune != 0f)
                {
                    timeBeforeTheReconquest -= Time.deltaTime;

                    if (timeBeforeTheReconquest <= 0)
                    {
                        timeBeforeTheReconquest = 0;
                    }

                    if (timeBeforeTheReconquest <= 0)
                    {
                        if (pointForCaptureRune < .5f)
                        {
                            ActiveLigtAndParticle(capturePointID);
                            pointForCaptureRune += loseRoneSpeed;
                        }
                        else if (pointForCaptureRune > .5f)
                        {
                            ActiveLigtAndParticle(capturePointID);
                            pointForCaptureRune -= loseRoneSpeed;
                        }
                    }
                }
            }
            else
            {
                pointForCaptureRune = .5f;
            }
        }
        
    }

    void ContestControl()
    {
        if (pointForCaptureRune <= 0)
        {
            pointForCaptureRune = 0;
        }
        else if (pointForCaptureRune >= 1)
        {
            pointForCaptureRune = 1;
        }
    }

    void CallNameOfChilds(int ID)
    {
        if (ID == 1)
        {
            if (pointForCaptureRune < 0.5f)
            {
                runeDisputed = gameObject.transform.GetChild(0).gameObject;
                gameObject.transform.GetChild(1).gameObject.SetActive(false);
            }
            else if (pointForCaptureRune > 0.5f)
            {
                runeDisputed = gameObject.transform.GetChild(1).gameObject;
                gameObject.transform.GetChild(0).gameObject.SetActive(false);
            }
        }
        else if (ID == 2)
        {
            if (pointForCaptureRune < 0.5f)
            {
                runeDisputed = gameObject.transform.GetChild(0).gameObject;
                gameObject.transform.GetChild(1).gameObject.SetActive(false);
            }
            else if (pointForCaptureRune > 0.5f)
            {
                runeDisputed = gameObject.transform.GetChild(1).gameObject;
                gameObject.transform.GetChild(0).gameObject.SetActive(false);
            }
        }
        else if (ID == 3)
        {
            if (pointForCaptureRune < 0.5f)
            {
                runeDisputed = gameObject.transform.GetChild(0).gameObject;
                gameObject.transform.GetChild(1).gameObject.SetActive(false);
            }
            else if (pointForCaptureRune > 0.5f)
            {
                runeDisputed = gameObject.transform.GetChild(1).gameObject;
                gameObject.transform.GetChild(0).gameObject.SetActive(false);
            }
        }
        else if (ID == 4)
        {
            if (pointForCaptureRune < 0.5f)
            {
                runeDisputed = gameObject.transform.GetChild(0).gameObject;
                gameObject.transform.GetChild(1).gameObject.SetActive(false);
            }
            else if (pointForCaptureRune > 0.5f)
            {
                runeDisputed = gameObject.transform.GetChild(1).gameObject;
                gameObject.transform.GetChild(0).gameObject.SetActive(false);
            }
        }
        else if (ID == 5)
        {
            if (pointForCaptureRune < 0.5f)
            {
                runeDisputed = gameObject.transform.GetChild(0).gameObject;
                gameObject.transform.GetChild(1).gameObject.SetActive(false);
            }
            else if (pointForCaptureRune > 0.5f)
            {
                runeDisputed = gameObject.transform.GetChild(1).gameObject;
                gameObject.transform.GetChild(0).gameObject.SetActive(false);
            }
        }
        ActiveLigtAndParticle(ID);

    }

    private void ActiveLigtAndParticle(int ID)
    {
        if (capturePointID == ID)
        {
            GameObject light = runeDisputed.transform.GetChild(1).gameObject;
            GameObject particle = runeDisputed.transform.GetChild(2).gameObject;
            GameObject glifo1 = runeDisputed.transform.GetChild(3).gameObject;
            GameObject glifo2 = runeDisputed.transform.GetChild(4).gameObject;
            GameObject glifo3 = runeDisputed.transform.GetChild(5).gameObject;
            GameObject glifo4 = runeDisputed.transform.GetChild(6).gameObject;
            GameObject glifo5 = runeDisputed.transform.GetChild(7).gameObject;
            GameObject glifo6 = runeDisputed.transform.GetChild(8).gameObject;
            GameObject glifo7 = runeDisputed.transform.GetChild(9).gameObject;
            GameObject glifo8 = runeDisputed.transform.GetChild(10).gameObject;
            //GameObject lightContest = runeDisputed.transform.GetChild(11).gameObject;

            if (playerID != actualPlayerID)
            {
                light.SetActive(false);
                particle.SetActive(false);
                runeDisputed.SetActive(false);
                actualPlayerID = playerID;
            }

            if (pointForCaptureRune != 0.5f)
            {
                runeDisputed.SetActive(true);
                light.SetActive(true);
            }
            if (pointForCaptureRune <= 0.5f && pointForCaptureRune >= 0.44445f || pointForCaptureRune >= 0.5f && pointForCaptureRune <= 0.55555f)
            {
                glifo1.SetActive(false);
            }
            else if (pointForCaptureRune <= 0.5f && pointForCaptureRune >= 0.3889f || pointForCaptureRune >= 0.5f && pointForCaptureRune <= 0.6111f)
            {
                glifo1.SetActive(true);
                glifo2.SetActive(false);
            }
            else if (pointForCaptureRune <= 0.5f && pointForCaptureRune >= 0.33335f || pointForCaptureRune >= 0.5f && pointForCaptureRune <= 0.66665f)
            {
                glifo2.SetActive(true);
                glifo3.SetActive(false);
            }
            else if (pointForCaptureRune <= 0.5f && pointForCaptureRune >= 0.2778f || pointForCaptureRune >= 0.5f && pointForCaptureRune <= 0.7222f)
            {
                glifo3.SetActive(true);
                glifo4.SetActive(false);
            }
            else if (pointForCaptureRune <= 0.5f && pointForCaptureRune >= 0.22225f || pointForCaptureRune >= 0.5f && pointForCaptureRune <= 0.77775f)
            {
                glifo4.SetActive(true);
                glifo5.SetActive(false);
            }
            else if (pointForCaptureRune <= 0.5f && pointForCaptureRune >= 0.1667f || pointForCaptureRune >= 0.5f && pointForCaptureRune <= 0.8333f)
            {
                glifo5.SetActive(true);
                glifo6.SetActive(false);
            }
            else if (pointForCaptureRune <= 0.5f && pointForCaptureRune >= 0.11115f || pointForCaptureRune >= 0.5f && pointForCaptureRune <= 0.88885f)
            {
                glifo6.SetActive(true);
                glifo7.SetActive(false);
            }
            else if (pointForCaptureRune <= 0.5f && pointForCaptureRune >= 0.0556f || pointForCaptureRune >= 0.5f && pointForCaptureRune <= 0.9444f)
            {
                glifo7.SetActive(true);
                glifo8.SetActive(false);
            }
            else if (pointForCaptureRune <= 0.5f && pointForCaptureRune >= 0f || pointForCaptureRune >= 0.5f && pointForCaptureRune <= 1f)
            {
                runeControlled = runeDisputed.GetComponentInChildren<Light>();
                glifo8.SetActive(true);
                lightEnableCapturd = true;
            }

            if (pointForCaptureRune >= 1 || pointForCaptureRune <= 0)
            {
                particle.gameObject.SetActive(true);
            }

        }
    }

    void ResetRune()
    {
        
        pointForCaptureRune = 0.5f;
        if (DisableThisRune != null)
        {
            DisableThisRune(capturePointID);
        }

        runeDisputed.transform.GetChild(1).gameObject.SetActive(false);
        runeDisputed.transform.GetChild(2).gameObject.SetActive(false);
        runeDisputed.transform.GetChild(3).gameObject.SetActive(false);
        runeDisputed.transform.GetChild(4).gameObject.SetActive(false);
        runeDisputed.transform.GetChild(5).gameObject.SetActive(false);
        runeDisputed.transform.GetChild(6).gameObject.SetActive(false);
        runeDisputed.transform.GetChild(7).gameObject.SetActive(false);
        runeDisputed.transform.GetChild(8).gameObject.SetActive(false);
        runeDisputed.transform.GetChild(9).gameObject.SetActive(false);
        runeDisputed.transform.GetChild(10).gameObject.SetActive(false);
        runeDisputed.SetActive(false);
    }
}

